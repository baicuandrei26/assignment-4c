import React from "react";
import Robot from "./Robot";
import RobotStore from '../stores/RobotStore'
class RobotForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id : 0,
	type : '',
	name : '',
	mass :''
    };
  }

  add = () => {
    if (this.validate()) {
      const robot = Object.assign({}, this.state);
      this.clearFields();
 
      this.props.onAdd(robot);
    }
  };

  validate = ()=>{
      
      if((this.state.name.length > 0) && (this.state.type.length>0) && (this.state.mass.length > 0) )
         return true;
      else 
        return false;
  }

  clearFields = () => {
    this.setState({
      name: "",
      type: "",
      mass: ""
    });
  };

  onChangeName = e => {

      this.setState({ name: e.target.value });
    
  };
  onChangeType = e => {
      this.setState({ type: e.target.value });
 
  };
  onChangeMass = e => {
      this.setState({mass: e.target.value });
  };
 


  render() {
    return (
      <div>
         <input type="button" value="add" onClick={() => this.props.onAdd({
                id : this.state.id,
                type : this.state.type,
                name : this.state.name,
                mass :this.state.mass
            })}/>

        <input
          id="name"
          type="text"
          placeholder="name"
          value={this.state.name}
          onChange={this.onChangeName}
        ></input>

        <input
          id="type"
          type="number"
          placeholder="type"
          value={this.state.type}
          onChange={this.onChangeType}
        ></input>

        <input
          id="mass"
          type="text"
          placeholder="mass"
          value={this.state.mass}
          onChange={this.onChangeMass}
        ></input>
        
      </div>
    );
  }
}

export default RobotForm;
